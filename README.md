# Arduino PubSub (Publisher/Subscriber) Design Pattern library.

An Arduino implementation of the Publisher/Subscriber design pattern for creating loosely coupled modules.

andrewzuku, retrojdm.com