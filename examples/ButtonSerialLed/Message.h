#pragma once

namespace Message
{
  const char BUTTON_UP[]   = "[BUP]";
  const char BUTTON_DOWN[] = "[BDN]";
};