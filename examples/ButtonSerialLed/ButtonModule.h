#pragma once

#include <Arduino.h>
#include <PubSub.h>
#include "Message.h"

const uint8_t BUTTON_PIN                = 30; // Pause button on an FP10 front panel.
const uint8_t BUTTON_DEBOUNCE_INTERVAL  = 20; // millis

class ButtonModule : public Publisher<char*>
{
 public:
  ButtonModule(Broker<char*> & broker);
  void begin();
  void loop();
  
 private:
  bool _buttonDown;
  bool _buttonWasDown;
  unsigned long _lastChange;
};

ButtonModule::ButtonModule(Broker<char*> & broker):
  Publisher<char*>(broker)
{
  _lastChange = 0;
}

void ButtonModule::begin()
{
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  _buttonDown = (digitalRead(BUTTON_PIN) == LOW);
  _buttonWasDown = _buttonDown;
}

void ButtonModule::loop()
{
  _buttonWasDown = _buttonDown;
  _buttonDown = (digitalRead(BUTTON_PIN) == LOW);
  unsigned long now = millis();

  if (now - _lastChange >= BUTTON_DEBOUNCE_INTERVAL)
  {
  
    if (_buttonDown && !_buttonWasDown)
    {
      _lastChange = now;
      publish(const_cast<char*>(Message::BUTTON_DOWN));
      return;
    }
  
    if (!_buttonDown && _buttonWasDown)
    {
      _lastChange = now;
      publish(const_cast<char*>(Message::BUTTON_UP));
      return;
    }
  }
}
