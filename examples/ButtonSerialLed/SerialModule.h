#pragma once

#include <PubSub.h>
#include "Module.h"

const uint16_t SERIAL_BAUD_RATE = 9600;
const char* const SERIAL_WELCOME_MESSAGE = "Serial Module subscribed to all messages.";

class SerialModule : public Subscriber<char*>
{
 public:
  SerialModule(Broker<char*> & broker);
  void begin();
  void loop();
  void notify(char* );
};

SerialModule::SerialModule(Broker<char*> & broker)
  : Subscriber<char*>(broker)
{
}

void SerialModule::begin()
{
  Serial.begin(SERIAL_BAUD_RATE);
  Serial.println(SERIAL_WELCOME_MESSAGE);
  subscribe(Module::All);
}

void SerialModule::loop()
{
}

void SerialModule::notify(char* message)
{
  Serial.println(message);
}
