#include <PubSub.h>
#include "ButtonModule.h"
#include "LedModule.h"
#include "SerialModule.h"

const size_t MESSAGE_SIZE = 64;
const size_t MESSAGE_BUFFER_SIZE = 16;
char gMessageBuffer[MESSAGE_BUFFER_SIZE][MESSAGE_SIZE];

const uint8_t SUBSCRIPTION_BUFFER_SIZE = 8;
Subscription<char*> gSubscriptionsBuffer[SUBSCRIPTION_BUFFER_SIZE];

// Topics are the first letter of the message.
uint8_t topicCallback(char* message) { message[0] == '[' ? message[1] : message[0]; }

// Simply print errors.
void errorCallback(const char* const error) { Serial.print("BROKER ERROR: "); Serial.println(error); }

// Our messages are simply strings (char* ).
Broker<char*> gBroker(
  (char* *)gMessageBuffer,
  MESSAGE_BUFFER_SIZE,
  gSubscriptionsBuffer,
  SUBSCRIPTION_BUFFER_SIZE,
  topicCallback,
  errorCallback);

ButtonModule gButton(gBroker);
LedModule gLed(gBroker);
SerialModule gSerial(gBroker);

void setup() {
  gButton.begin();
  gSerial.begin();
  gLed.begin();
}

void loop() {
  gButton.loop();
  gSerial.loop();
  gLed.loop();
  gBroker.distribute();
}
