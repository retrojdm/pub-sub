#pragma once

#include <Arduino.h>

namespace Module
{
  const uint8_t All     = 0;
  const uint8_t Buttons = 'B';
  const uint8_t Led     = 'L';
  const uint8_t Serial  = 'S';
};
