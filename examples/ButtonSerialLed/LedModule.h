#pragma once

#include <PubSub.h>
#include "Message.h"
#include "Module.h"

const uint8_t LED_PIN = 13; // Pause LED on an FP10 front panel.

class LedModule : public Subscriber<char*>
{
 public:
  LedModule(Broker<char*> & broker);
  void begin();
  void loop();
  void notify(char* message);
};

LedModule::LedModule(Broker<char*> & broker):
  Subscriber<char*>(broker)
{
}

void LedModule::begin()
{
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  subscribe(Module::Buttons);
}

void LedModule::loop()
{ 
}

void LedModule::notify(char* message)
{
  // We have to copy the message before using strtok, because it mutates the input string,
  // and we need to preserve it for other subscribers.
  char tokens[64];
  strcpy(tokens, message);
  char* code = strtok(tokens, " ");

  if (strcmp(code, Message::BUTTON_UP) == 0)
  {
    digitalWrite(LED_PIN, LOW);
    return;
  }

  if (strcmp(code, Message::BUTTON_DOWN) == 0)
  {
    digitalWrite(LED_PIN, HIGH);
    return;
  }
}
