#!/bin/bash

if [ -d "./build" ]; then
    rm ./build -rf
fi

cmake -S . -B build -G "Ninja" -Wno-dev