#include "TestModule.h"

TestModule::TestModule(Broker<char*> & broker):
  Publisher<char*>(broker),
  Subscriber<char*>(broker),
  lastNotification("")
{
}

void TestModule::begin()
{
}

void TestModule::loop()
{ 
}

void TestModule::notify(char* message)
{
  strcpy(lastNotification, message);
}