#include <gtest/gtest.h>

#include "../src/PubSub.h"
#include "TestModule.h"

const size_t MESSAGE_SIZE = 256;
const size_t MESSAGE_BUFFER_SIZE = 4;
const uint8_t SUBSCRIPTION_BUFFER_SIZE = 4;

class TestFixture : public testing::Test {
protected:
  char messageBuffer[MESSAGE_BUFFER_SIZE][MESSAGE_SIZE];
  Subscription<char*> subscriptionsBuffer[SUBSCRIPTION_BUFFER_SIZE];
  Broker<char*> broker;

  TestModule testModuleA;
  TestModule testModuleB;
  TestModule testModuleC;
  TestModule testModuleD;

  TestFixture() :
    broker(
      (char* *)messageBuffer,
      MESSAGE_BUFFER_SIZE,
      subscriptionsBuffer,
      SUBSCRIPTION_BUFFER_SIZE,
      [](char* message) -> uint8_t { return message[0]; }, // Topic is first letter.
      [](const char* const error) { std::cout << "BROKER ERROR: " << error; }),

    testModuleA(broker),
    testModuleB(broker),
    testModuleC(broker),
    testModuleD(broker)
  {}

  void SetUp() override
  {
    // In real code, modules would subscribe to other modules within their own .begin() functions, but we're doing it
    // here to make the tests more transparent and understandable. We're not testing the modules, after all.
    //
    // Note: we're indtentionally not subscribing testModuleD to anything.
    testModuleA.subscribe('A');
    testModuleB.subscribe('B');
    testModuleC.subscribe('C');    
  }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Broker tests

TEST_F(TestFixture, Broker_distribute)
{
  EXPECT_EQ(strlen(testModuleA.lastNotification), 0);
  EXPECT_EQ(strlen(testModuleB.lastNotification), 0);
  EXPECT_EQ(strlen(testModuleC.lastNotification), 0);

  broker.distribute();
  
  EXPECT_EQ(strlen(testModuleA.lastNotification), 0);
  EXPECT_EQ(strlen(testModuleB.lastNotification), 0);
  EXPECT_EQ(strlen(testModuleC.lastNotification), 0);

  testModuleA.publish((char *)"Bee");
  broker.distribute();

  EXPECT_EQ(strlen(testModuleA.lastNotification), 0);
  EXPECT_STREQ(testModuleB.lastNotification, "Bee");
  EXPECT_EQ(strlen(testModuleC.lastNotification), 0);
}

TEST_F(TestFixture, Broker_available)
{
  EXPECT_FALSE(broker.available());
  testModuleA.publish((char *)"Bee");
  EXPECT_TRUE(broker.available());
  broker.distribute();
  EXPECT_FALSE(broker.available());
}

TEST_F(TestFixture, Broker_publish)
{
  EXPECT_FALSE(broker.available());
  broker.publish((char*)"Cat");
  EXPECT_TRUE(broker.available());
}

TEST_F(TestFixture, Broker_subscribe)
{
  EXPECT_NE(subscriptionsBuffer[3].pSubscriber, &testModuleD);
  EXPECT_NE(subscriptionsBuffer[3].topic, 'D');

  Subscription subscription(&testModuleD, 'D');
  broker.subscribe(subscription);

  EXPECT_EQ(subscriptionsBuffer[3].pSubscriber, &testModuleD);
  EXPECT_EQ(subscriptionsBuffer[3].topic, 'D');
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Publisher tests

TEST_F(TestFixture, Publisher_publish)
{
  EXPECT_FALSE(broker.available());
  testModuleA.publish((char*)"Apple");
  EXPECT_TRUE(broker.available());
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Subscriber tests

TEST_F(TestFixture, Subscriber_subscribe)
{
  // Note: We've already called .subscribe() on each module in SetUp();

  EXPECT_EQ(subscriptionsBuffer[0].pSubscriber, &testModuleA);
  EXPECT_EQ(subscriptionsBuffer[0].topic, 'A');

  EXPECT_EQ(subscriptionsBuffer[1].pSubscriber, &testModuleB);
  EXPECT_EQ(subscriptionsBuffer[1].topic, 'B');

  EXPECT_EQ(subscriptionsBuffer[2].pSubscriber, &testModuleC);
  EXPECT_EQ(subscriptionsBuffer[2].topic, 'C');
}
