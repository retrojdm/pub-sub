#pragma once

#include <cstddef>
#include <cstring>
#include "../src/PubSub.h"

const size_t MESSAGE_MAX_SIZE = 256;

class TestModule :
  public Publisher<char*>,
  public Subscriber<char*>
{
public:
  TestModule(Broker<char*> & broker);
  void begin();
  void loop();
  void notify(char* message);

  char lastNotification[MESSAGE_MAX_SIZE];
};
