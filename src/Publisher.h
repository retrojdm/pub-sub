#pragma once

#include "Broker.h"

namespace pubsub
{
  template<typename T>
  class Publisher
  {
  private:
    Broker<T> & _broker;

  public:
    Publisher(Broker<T> & broker) : _broker(broker) {}
    
    void publish(T message)
    {
      _broker.publish(message);
    }
  };
}
