#pragma once

#if defined(ARDUINO)
#include <Arduino.h>
#else
#include <cstdint>
#include <cstddef>
#endif

#include "Subscription.h"

namespace pubsub
{
  template<typename T>
  class Broker;

  template<typename T>
  class Subscriber
  {
  private:
    Broker<T> & _broker;

  public:
    Subscriber(Broker<T> & broker): _broker(broker) {}

    void subscribe(const uint8_t topic)
    {
      Subscription<T> subscription(this, topic);
      _broker.subscribe(subscription);
    }

    virtual void notify(T message) = 0;
  };
}