#pragma once

// https://gitlab.com/retrojdm/ring-buffer
#include <RingBuffer.h>

#include "Subscription.h"
#include "Subscriber.h"

namespace pubsub
{
  static const char* const MAX_MESSAGES_ERROR = "Can't add any more messages.";
  static const char* const MAX_SUBSCRIPTIONS_ERROR = "Can't add any more subscriptions.";

  // The Topic callback should derive a topic from a message.
  template<typename T>
  using TopicCallback = uint8_t (*)(T);

  // The optional error callback is called by the PubSub system when there's an error.
  using ErrorCallback = void (*)(const char* const);

  // The optional publish callback is called when a new message is published.
  template<typename T>
  using PublishCallback = void (*)(T);
  
  template<typename T>
  class Broker
  {
  public:
    RingBuffer<T> messageQueue;

  private:
    size_t _maxSubscriptions;
    Subscription<T>* _subscriptions;
    size_t _subscriptionCount;
    TopicCallback<T> _topicCallback;
    ErrorCallback _errorCallback;
    PublishCallback<T> _publishCallback;

  public:
    Broker(
      T* messageQueueBuffer,
      const size_t messageQueueBufferSize,
      Subscription<T>* subscriptionsBuffer,
      const size_t subscriptionsBufferSize,
      TopicCallback<T> topicCallback,
      ErrorCallback errorCallback = nullptr,
      PublishCallback<T> publishCallback = nullptr)
      :
      messageQueue(messageQueueBuffer, messageQueueBufferSize),
      _maxSubscriptions(subscriptionsBufferSize),
      _subscriptions(subscriptionsBuffer),
      _subscriptionCount(0),
      _topicCallback(topicCallback),
      _errorCallback(errorCallback),
      _publishCallback(publishCallback) {}

    // Should be called in your sketch's main loop.
    // Tries to pop a single message off the queue, and distribute it to any subscribers.
    void distribute()
    {
      if (messageQueue.isEmpty())
      {
        return;
      }

      bool notified[_subscriptionCount] = {false};

      T message = messageQueue.pop();
      uint8_t messageTopic = _topicCallback(message);

      for (uint8_t i = 0; i < _subscriptionCount; i++)
      {
        Subscription<T> subscription = _subscriptions[i];
        if ((subscription.topic == 0) || (subscription.topic == messageTopic))
        {
          if (!notified[i])
          {
            subscription.pSubscriber->notify(message);
            notified[i] = true;
          }
        }
      }
    }

    // Returns a value indicating whether the queue still has messages on it.
    bool available()
    {
      return !messageQueue.isEmpty();
    }

    // Tries to push a message to the queue.
    void publish(T message)
    {
      if (_publishCallback != nullptr)
      {
        _publishCallback(message);
      }

      bool success = messageQueue.push(message);
      if (!success && _errorCallback != nullptr)
      {
        _errorCallback(MAX_MESSAGES_ERROR);
      }
    }

    // Tries to register a subscription.
    void subscribe(Subscription<T> subscription)
    {
      if (_subscriptionCount == _maxSubscriptions)
      {
        _errorCallback(MAX_SUBSCRIPTIONS_ERROR);
        return;
      }

      _subscriptions[_subscriptionCount] = subscription;
      _subscriptionCount++;
    }
  };

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Specialisation for pointer types.
  // See: https://learn.microsoft.com/en-us/cpp/cpp/template-specialization-cpp?view=msvc-170
  //
  // TODO: Could this be partial specialistion? Do we really need to duplicate all that code?
  template<typename T>
  class Broker<T*>
  {
  public:
    RingBuffer<T*> messageQueue;

  private:
    size_t _maxSubscriptions;
    Subscription<T*>* _subscriptions;
    size_t _subscriptionCount;
    TopicCallback<T*> _topicCallback;
    ErrorCallback _errorCallback;
    PublishCallback<T*> _publishCallback;

  public:
    Broker(
      T** messageQueueBuffer,
      const size_t messageQueueBufferSize,
      Subscription<T*>* subscriptionsBuffer,
      const size_t subscriptionsBufferSize,
      TopicCallback<T*> topicCallback,
      ErrorCallback errorCallback = nullptr,
      PublishCallback<T*> publishCallback = nullptr)
      :
      messageQueue(messageQueueBuffer, messageQueueBufferSize),
      _maxSubscriptions(subscriptionsBufferSize),
      _subscriptions(subscriptionsBuffer),
      _subscriptionCount(0),
      _topicCallback(topicCallback),
      _errorCallback(errorCallback),
      _publishCallback(publishCallback) {}

    // Should be called in your sketch's main loop.
    // Tries to pop a single message off the queue, and distribute it to any subscribers.
    // Optionally deallocate the message memory when done, to free up the memory.
    void distribute(bool deallocate = false)
    {
      if (messageQueue.isEmpty())
      {
        return;
      }

      bool notified[_subscriptionCount] = {false};

      T* pMessage = messageQueue.pop();
      uint8_t messageTopic = _topicCallback(pMessage);

      for (uint8_t i = 0; i < _subscriptionCount; i++)
      {
        Subscription<T*> & subscription = _subscriptions[i];

        if ((subscription.topic == 0) || (subscription.topic == messageTopic))
        {
          if (!notified[i])
          {
            subscription.pSubscriber->notify(pMessage);
            notified[i] = true;
          }
        }
      }

      if (deallocate)
      {
        delete pMessage;
      }
    }

    // Returns a value indicating whether the queue still has messages on it.
    bool available()
    {
      return !messageQueue.isEmpty();
    }

    // Tries to push a message to the queue.
    void publish(T* pMessage)
    {
      if (_publishCallback != nullptr)
      {
        _publishCallback(pMessage);
      }

      bool success = messageQueue.push(pMessage);
      if (!success && _errorCallback != nullptr)
      {
        _errorCallback(MAX_MESSAGES_ERROR);
      }
    }

    // Tries to register a subscription.
    void subscribe(Subscription<T*> subscription)
    {
      if (_subscriptionCount == _maxSubscriptions)
      {
        _errorCallback(MAX_SUBSCRIPTIONS_ERROR);
        return;
      }

      _subscriptions[_subscriptionCount] = subscription;
      _subscriptionCount++;
    }
  };
}