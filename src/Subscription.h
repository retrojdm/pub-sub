#pragma once

#if defined(ARDUINO)
#include <Arduino.h>
#else
#include <cstdint>
#include <cstddef>
#endif

namespace pubsub
{
  template<typename T>
  class Subscriber;

  template<typename T>
  class Subscription
	{
  public:
    Subscription();
    Subscription(Subscriber<T> * pSubscriber, const uint8_t topic);

	  Subscriber<T> * pSubscriber;
	  uint8_t topic;
	};

  template<typename T>
  Subscription<T>::Subscription()
  {
    this->pSubscriber = nullptr;
    this->topic = 0;
  }

  template<typename T>
  Subscription<T>::Subscription(Subscriber<T> * pSubscriber, const uint8_t topic)
  {
    this->pSubscriber = pSubscriber;
    this->topic = topic;
  }
}
